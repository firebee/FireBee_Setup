                "FT-Standard" 32pts - Icon set for FireTOS
               --------------------------------------------
                Version: 1.00 (distribution - 2019/05/15)
   

The FT-standard" icon set is composed by 60 icons (32 pts / 16 colors) and
was created for the FireBee computer to replace the default FireTOS icons. 

Although not required, the use of the NVDI palette is recommended.

Copy the deskicon.rsc in the root of your boot drive (usually C:\) and 
reboot your FireBee. FireTOS will use the FT-standard icons now. Icons 1-6 
are assigned per default to drawer, folder, trash, program, file and printer.  

You can assign other icons (or change default ones) via the "install icons" 
item in the Options Menu. Before powering off / reboot your FireBee, remember 
to use the save desktop option to permanently store your settings (icon 
assignments, desktop disposition, windows size/position, screen res., etc) 
in the NEWDESK.INF file. 
 
Copyrights
----------
All icons: design by Lodovico Zanier (Ldv01)

Release date : 2019/05/15

License
-------
This icons set is licensed by the Author listed in the Copyrights section 
of this readme file, under a:

CREATIVE COMMONS  ATTIBUTION - NONCOMMERCIAL  - NO DERIVATES 3.0  UNPORTED
LICENSE (http://creativecommons.org/licenses/by-nc-nd/3.0/)

Under this licence, you are free:

to Share - to copy, distribute and transmit the work under the following
conditions:

Attribution - You must attribute the work in the manner specified by  the
author or licensor (but not in any way that suggests that they endorse you
or your use of the work).

Noncommercial - You may not use this work for commercial purposes. 

No Derivative Works - You may not alter, transform, or build upon this
work. 

The full Creative Commons License is in the CC_Legal.txt document. Please
note that the summary.txt file, also included, has no legal value, and is 
supplied to help to understand the  full Legal Code (CC_Legal.txt).

Archive contents
----------------
deskicon.rsc  : Color icons RSC file (60 icons, available in 16 & 2 colors);
readme.txt    : This file;
summary.txt   : Handy reference for understanding the Legal Code (the full
                license in CC_Legal.txt);
CC_Legal.txt  : Creative Commons license (CC BY-NC-ND).

DISCLAIMER
----------
This Icon Set is PROVIDED  AS IT IS, and  the  author DISCLAIM ALL
RESPONSIBILITY FOR ANY LOSS OR DAMAGE resulting from the use, no matter
how it is caused.



