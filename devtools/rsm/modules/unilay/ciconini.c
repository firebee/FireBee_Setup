/*************************************************************
 *                                                           *
 *     Examples for cicon_init() and cicon_exit()            *
 *     Author: Armin Diedering                               *
 *     WWW: http://www.Diedering.de                          *
 *                                                           *
 *************************************************************/

#include <portab.h>
#include <gemfast.h>


/* Following functions are required */

WORD CountOfPlanes();		/* returns the number of planes of the  */
									/*	current resolution  */

WORD CanCicon();			/* gives info if the system can do CICONs */

WORD IsMagiC();			/* is MagiC the OS ?                      */


/* now we are off! */

long rs_ciconinit(CICONBLK *ciconblks, int ncib, OBJECT *objects, int nobj)
{
	if(CanCicon())
	{
		if(IsMagiC())
		{
			while(ncib--)                           /* see rsm2cs.hyp  */
			{
				CICON *cicon = ciconblks->mainlist;
				CICON *found = cicon;
				WORD planes = 0;
				while(cicon)                   /* find suitable colour number */
				{
					if(cicon->num_planes <= CountOfPlanes()) /* can the OS handle this icon? */
					{
						if(cicon->num_planes > planes)        /* are there more planes   */
						{                                     /* that I have found already? */
							found		= cicon;
							planes	= found->num_planes;      /* I'll remember this    */
						}
					}
					cicon = cicon->next_res;                 /* next colour number    */
				}
				ciconblks->mainlist = found;                /* set found             */
				ciconblks++;                                /* next icon             */
			}
		}
		else
		{
			/* Here it gets complicated. One has to go through CICON by CICON.       */
			/* For this you first have to find and set a suitable colour number      */
			/* as for MagiC see above). Then the found image data, which are present */
			/* in the standard format, have to be converted to the device-specific   */
			/* format.                                                               */
			/* How this is to be achieved I'll leave to you!  ;-)                    */
		}
	}
	else					/* the system can not handle CICONs */
	{
		while(nobj--)       /* then convert all CICONs to ICONs  */
		{
			if((objects->ob_type & 0xff) == G_CICON)
				objects->ob_type = (objects->ob_type & ~0xff) | G_ICON;
			objects++;
		}
	}
	return(0);
}

void rs_ciconexit(long deskriptor)
{
}
