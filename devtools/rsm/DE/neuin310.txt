Was gibt es denn nun neues beim ResourceMaster v3.1:

Allgemeine Bedienung:

Alle PopUps lassen sich jetzt wahlweise Mac-like bedienen. 
Das Hei�t:
  - PopUp-Butten anklicken (aber die Maustaste dabei festhalten)
  - Eintrag ausw�hlen
  - Maustate loslassen
  - fertig
  
oder

  - PopUp-Butten normal anklicken 
  - Eintrag ausw�hlen
  - Eintrag anklicken
  - fertig


Vorlagen:

Eine Vorlage gab es zwar schon bei der Version 3.0 (die '_new.rsc') aber ab 
der Version 3.1 kann jetzt jede beliebige Resource als Vorlage geladen 
werden. Verantwortlich daf�r ist der neue Men�eintrag "Vorlage �ffnen..." im 
Datei-Men�. Der Ordner 'patterns' im ResourceMaster-Verzeichnis wird dabei 
voreingestellt.
Der unterschied zwischen das normale �ffnen und das �ffnen einer Vorlage ist 
nur der, da� die Resource nach dem Laden in "noname.rsc" umbenannt wird.

ACHTUNG: Die Vorlage f�r Datei/Neu (die '_new.rsc') wird nicht meh im 
         'modules'-Ordner sondern im 'patterns'-Ordner gesucht.


Objekt-Koordinaten:

Der ResourceMaster arbeitet jetzt immer im Koordinatenformat der Resource 
(Zeichensatzraster + Pixelanteil). Abweichende Koordinaten bestehender 
Resourcen bleiben dadurch erhalten. Da dies nur dazu gedacht ist 
unterschiedliche Textl�ngen der verschiedenen Sprachen auszugleichen 
unterliegen die abweichenden Objekt-Positionen folgender Einschr�nkungen:
 - es kann nur die XX-Position und die Breite der Objekte je Layer 
   abweichen.
 - die Abweichung kann nur im "ganzen" Zeichensatzraster erfolgen.

Die X-Position wird dabei als relativer Wert zum Standard-Layer 
gespeichert. Wenn also die Position des Standard-Layers ver�ndert wird, 
dann wird auch die Position der anderen Layer mitver�ndert; allerdings 
relativ. 
Normalerweise ist die Breite auch relativ zur Breite des Standard-Layers. 
Dieser Zustand l��t sich daran erkennen, da� der Mauszeiger am Objektrand 
(Mausform: Size) nicht schwarz mit wei�em Rand sondern wei� mit schwarzem 
Rand erscheint. Wird aber die Breite ver�ndert wird vom Relativ- in den 
Absolut-Modus umgeschaltet (Mauszeiger ist wie gewohnt schwarz). In den 
Relativ-Modus gelangt man wieder, in dem man im Dialog-Editor-PopUp �ber 
"Gr��e �ndern" die Breite auf 0 setzt.

Da das ResourceMaster-Tooming-Verfahren zu der neuen Koordinatenbehandlung 
im Widerspruch steht existiert jetzt das ResourceMaster-Moving-Verfahren. 
Mehr zum -Zooming- und -Mooving-Verfahren steht im Hypertext.


Multilayer-Resourcen:

Multilayer-Resourcen sind prim�r zur parallelen Entwicklung mehsprachiger 
Resourcen gedacht. Da Texte, Button-Beschriftungen usw. in den jeweiligen 
Sprachen eine unterschiedliche L�nge haben k�nnen, hat der Resourcemaster 
die Objekte bisher so gro� gemacht, da� der auch der l�ngste Text hinein 
pa�te. Ab jetzt kann der ResourceMaster f�r jedes Objekt und f�r jeden Layer 
abweichende Koordinaten verarbeiten. Dadurch kann die Resource f�r jeden 
Layer individuell angepa�t werden. Au�erdem m�ssen die Men�s nicht mehr 
nachbearbeitet werden.
Auf wunsch kann man jetzt alle Layer automatisch exportieren. Dabei stehen 
zwei Methoden zur Verf�gung.
Au�erdem l��t sich bei den Ausgabedateien einstellen, ob sie immer (sinnvoll 
z.B. bei BGH-Dateien) oder ob sie nur f�r die Haupt-Resource erzeugt werden 
sollen


Objektgr��en-Pr�fung:

Bisher konnte man z.B. Buttons nur so weit verkleinern, da� der Button-Text 
gerade noch so hinein pa�te. Es gibt Programmierer, die verwenden den Text 
dazu, um z.B. Informationen f�r USERDEF-Objekte darin abzulegen. Jede 
�nderung des Textes hatte bisher die Folge, da� das Objekt in der Gr��e 
angepa�t wurde. Diese Verhalten l��t sich jetzt �ber Einstellung/Diverses... 
abschalten.


Ebenen:

Wer schon einmal versucht hat Karteikarten zu Entwerfen wird wissen, welch 
enormer Aufwand das bedeutet. Jetzt kann man jedes Objekt einer von 9 Ebenen 
zuordnen. Auf Knopfdruck kann man eine Ebene Zeigen und alle anderen 
Verstecken. Mehr dazu steht im Hypertext.


Image-/Icon-Editor:

Die Optik des Image-/Icon-Editors wurde etwas �berarbeitet. So kann man 
jetzt sofort erkennen, f�r welche Farbtiefen bereits Icons vorhanden sind.
Der IMG-Import ist jetzt ein XIMG-Import. Die Farbtabelle wird nach der 
"�hnlichste-Farbe-Methode" ausgewertet.
Au�erdem ist jetzt der (X)IMG-Import auch wieder unter True- bzw. Hi-Color 
m�glich.


Sonstiges:

Es sind auch noch weitere Neuerungen hinzugekommen, die aber eher interne 
Auswirkungen haben und nicht nach au�en treten.

