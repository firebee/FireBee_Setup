/*************************************************************
 *                                                           *
 *     Beispiele f�r cicon_init() und cicon_exit()           *
 *     Autor: Armin Diedering                                *
 *     WWW: http://www.Diedering.de                          *
 *                                                           *
 *************************************************************/

#include <portab.h>
#include <gemfast.h>


/* Folgende Funktionen werden Ben�tigt */

WORD CountOfPlanes();		/* liefert die Anzahl der Planes der aktuellen */
									/*	Aufl�sung zur�ck                            */

WORD CanCicon();				/* gibt Auskunft, ob das System CICONs kann    */

WORD IsMagiC();				/* Ist MagiC das OS ?                          */


/* Jetzt gehts aber los */

long rs_ciconinit(CICONBLK *ciconblks, int ncib, OBJECT *objects, int nobj)
{
	if(CanCicon())
	{
		if(IsMagiC())
		{
			while(ncib--)                                  /* siehe rsm2cs.hyp         */
			{
				CICON *cicon = ciconblks->mainlist;
				CICON *found = cicon;
				WORD planes = 0;
				while(cicon)                                /* passende Farbzahl suchen */
				{
					if(cicon->num_planes <= CountOfPlanes()) /* kann das OS dieses Icon? */
					{
						if(cicon->num_planes > planes)        /* sind es mehr Planes als  */
						{                                     /* ich schon gefunden habe? */
							found		= cicon;
							planes	= found->num_planes;      /* das merk ich mir         */
						}
					}
					cicon = cicon->next_res;                 /* n�chste Farbzahl         */
				}
				ciconblks->mainlist = found;                /* gefundesnes setzen       */
				ciconblks++;                                /* n�chstes Icon            */
			}
		}
		else
		{
			/* Hier wirds jetz aber kompliziert. man mu� CICON f�r CICON durchgehen. */
			/* Dabei sollte man erst einmal die passende Farbzahl (�hnlich wie bei   */
			/* MagiC s.o.) suchen und setzen. Dann m�ssen die gefundenen Bilddaten,  */
			/* die ja im Standrd-Format vorliegen in das ger�teabh�ngige Format      */
			/* gewandelt werden.                                                     */
			/* wie das zu bewerkstelligen ist �berlasse ich eurer Fantasie ;-)       */
		}
	}
	else					/* das System kann gar keine CICONs */
	{
		while(nobj--)  /* dann alle CICONs in ICONs �ndern */
		{
			if((objects->ob_type & 0xff) == G_CICON)
				objects->ob_type = (objects->ob_type & ~0xff) | G_ICON;
			objects++;
		}
	}
	return(0);
}

void rs_ciconexit(long deskriptor)
{
}
