#ifndef _OVL_H
#define _OVL_H

typedef LONG RSC_H;

typedef struct
{
	WORD		Protokoll;
	
	/* ab RSM2OVL.Protokoll >= 1 */

	RSC_H		(*RscLoad)(const char* RscFile, LONG Mode);
	void		(*RscFree)(RSC_H ResourceHandle);
	OBJECT*	(*RscGetTree)(RSC_H ResourceHandle, LONG Index);
	char*		(*RscGetSring)(RSC_H ResourceHandle, LONG Index);
	BITBLK*	(*RscGetImage)(RSC_H ResourceHandle, LONG Index);
	
	void		(*ConvertTree)(OBJECT* Tree, LONG Mode);
} RSM2OVL;



typedef struct
{
	OBJECT*	DialToolbox;		/* Die Toolbox f�r Dialoge                  */
	OBJECT*	MenuToolbox;		/* Die Toolbox f�r Men�s                    */
	OBJECT*	MenuDrop;			/* Wird im Men� ein Title eingef�gt,        */
										/* dann wird dieser Baum als Drop verwendet.*/
	OBJECT*	StdDialog;			/* Soll ein neuer Dialog angelegt werden,   */
										/* dann wird dieser Baum verwendet.         */
	OBJECT*	StdMenu;				/* Soll ein neues Men� angelegt werden,     */
										/* dann wird dieser Baum verwendet.         */
} TOOLS;

typedef WORD			(*EVENTHANDLER)(EVNT* Evnt);
typedef WORD			(*INITEVENTHANDLER)(EVENTHANDLER EventHandler);
typedef const char*  (*GETOBNAME)(LONG Idx);

typedef struct
{
	LONG		Magic;		/* 0x4f424a43 'OBJC' */
	WORD		Protokoll;
	LONG		ID;
	WORD		Version;
	char		Name[32];
	char		Copyright[32];

	WORD		(*Init)(const char* FileName, UWORD* Global, RSM2OVL* RsmOvl, TOOLS* Tools);
	void		(*Exit)();

	WORD		(*Draw)(OBJECT* Objc, LONG* Clip, LONG Flags);

	/* ab OVL2RSM.Protokoll >= 1 */

	WORD		(*GetMinSize)(OBJECT* Objc, LONG Flags, WORD* MinW, WORD* MinH);
	void		(*GetArea)(OBJECT* Objc, LONG Flags, GRECT* Area);
	
	WORD		(*SetUp)(INITEVENTHANDLER InitEventHandler);
	
	/* ab OVL2RSM.Protokoll >= 2 */
	
	WORD		(*Test)(LONG Mode, LONG Resource, LONG Idx, GETOBNAME GetObName, INITEVENTHANDLER InitEventHandler);

	LONG		(*GetShortCut)(OBJECT* Objc, LONG Flags, char ShortCut[10]);

} OVL2RSM;


#endif /* _OVL_H */
