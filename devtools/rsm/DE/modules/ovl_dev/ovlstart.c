#include <portab.h>
#include <osbind.h>
#include <string.h>
#include <basepage.h>
#include <gemfast.h>
#include "ovl.h"


extern OVL_RSM Overlay;

char Cmp[] = "RSMOVL";
WORD start(BASEPAGE* BasePage)
{
	WORD Ret = 1;

	if(BasePage->p_cmdlin[0] == 14)
	{
		if(!strncmp(&BasePage->p_cmdlin[1], Cmp, 6))
		{
			LONG Adr		= 0;
			char* Pos	= &BasePage->p_cmdlin[7];
			WORD i;

			for(i=0; i<8; i++)
			{
				Adr <<= 4;
				Adr |= *Pos++ - 'A';
			}
			*((OVL2RSM**)Adr)	= &Overlay;
			Ret = 0;
		}
	}
	Pterm(Ret);
}