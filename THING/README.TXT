=======================================================================
Thing, 1.27                                                  README.TXT
Copyright (c) 1994-98 Arno Welzel and Thomas Binder

TOS2GEM and ThingWait (c) Thomas Binder
'Dcheneva' fonts (c) Thomas Schulze (ts@uni-paderborn.de)

UK support by Joe Connor of InterActive - see SUPPORT.TXT for details.
======================================================================

Thing is Shareware! More about this in the file REGISTER.TXT.

Many thanks!


Actually an update for Thing with some bugfixes was due to appear soon 
after version 1.20, but unfortunately -- once again -- nothing came of 
this. The newly available version 1.27 is in comparison to 1.20 far more 
than a bugfix release and lives up to the larger jump of the version 
number.

The update is free of charge for registered users of Thing 1.20, but for
registered users of Version 1.0x there is a small charge for upgrading - 
see also UPDATE.TXT! Owners of the Psi-CD from the Whiteline CD Series can 
also update; the special conditions for this can be found in the file 
README.PSI!

The 'Thing Icon Manager' is ready at last, but I found it too big to 
include in the main Thing archive -- so it has its own archive which also 
makes separate updates easier. ThingIcn 1.10 should be available from the 
same source as Thing itself (if you don't have it already); the archive 
name is TICN110D.LZH for the German version and TICN110E.LZH for the 
English language one.


Contact addresses:
-----------------



  Continued development / contact for registrations, problems and 
  suggestions:

  Thomas Binder
  Johann-Valentin-May-Stra�e 7
  64665 Alsbach-H�hnlein

  EMail: binder@rbg.informatik.tu-darmstadt.de (InterNet)
         gryf@hrzpub.tu-darmstadt.de (ditto)
         Thomas Binder @ HD (MausNet, no mails > 16K!)
  IRC: Gryf
  Bank account:   Thomas Binder
                  Dresdner Bank AG Frankfurt am Main
                  Account No.: 9 024 050 00
                  BLZ (Sort code): 500 800 00


  Original author (no longer supporting Thing):

  Arno Welzel
  Georgenstra�e 55
  86152 Augsburg

  EMail: welzel@augusta.de


ST-Guide is available from:
----------------------------
  Holger Weets
  Tangastra�e 45
  26121 Oldenburg

  EMail: Holger Weets @ OL (MausNet, No mails > 16K!)
  Holger_Weets@ol.maus.de (InterNet, same restriction as above!)

Or once again from Joe Connor of InterActive -- see SUPPORT.TXT.

  I have to note that I have no financial or other connection with 
  St-Guide other than as a satisfied user!
