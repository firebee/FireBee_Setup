Contents of 'Thing 1.27' package
--------------------------------

ARCHIV.GRP .......... Thing object group with important files
CONTENTS.TXT ........ This file ;-)
NEW.TXT ............. Important information about new version
README.PSI .......... Update notes for users of the Psi-CD full version
README.TXT .......... ReadMe
REGISTER.TXT ........ Registration
SUPPORT.TXT ......... UK Software Support for english speaking users
THIN127E.UPL ........ Upload text for BBS, mail-boxes etc
UPDATE.TXT .......... Update conditions

APFELIMG\ ........... Fractal tile module for Thing - see APFELIMG.TXT
  APFEL020.OVL
  APFELIMG.INF
  APFELIMG.OVL
  APFELIMG.TXT

IMAGES\ ............. Some background images for Thing
  ATARI.IMG
  BACKGND1.IMG
  BACKGND2.IMG
  BACKGND3.IMG
  BACKGND4.IMG
  DESKTOP.IMG
  INTEL.IMG
  THING16.IMG
  THING256.IMG

DOC\
  ICONS.DOC ......... List of icons included with Thing
  THING.HYP ......... Hypertext documentation for Thing (unfortunately 
  THING.REF           not yet quite complete)
  THINGIMG.TXT ...... Documentation for ThingImg by Thomas K�nneth
  VAPROTO.H ......... Current header file for the AV-Protocol


GEMSYS\ ............. Some GDOS-fonts, partly by Thomas Schulze
  DCHENM05.FNT
  DCHENM07.FNT
  DCHENM11.FNT
  DCHENP07.FNT
  DCHENP10.FNT
  DCHENP13.FNT
  DCHENP17.FNT
  THING04.FNT
  THING10.FNT

THING\
  ICONS.INF ......... Icon assignments
  ICONS.RSC ......... Icons, colour
  MEDICON.RSC ....... Icons, 32*16 monochrome
  MONOICON.RSC ...... Icons, monochrome
  THING.APP ......... Thing executable
  THING.RSC ......... Thing resource file with dialogs
  THINGCOL.RSC ...... Alternative to THING.RSC with coloured logo
  THINGTXT.RSC ...... Thing resource file with alert texts etc.
  THINGICN.TXT ...... Note about availability of THINGICN.APP
  THINGIMG.OVL ...... External (X)IMG loading module by Thomas K�nneth
  THINGREG.OVL ...... Module for checking the registration
  THINGRUN.PRG ...... ThingRun for unloading Thing (as overlay)

THINWAIT\ ........... A utility for Thing under Single-TOS - see
  THINWAIT.APP        THINWAIT.TXT
  THINWAIT.TXT

TOS2GEM\ ............ TOS2GEM basic version - see TOS2GEM.TXT
  T2GRESET.PRG
  T2G_INTR.PRG
  TOS2GEM.PRG
  TOS2GEM.TXT
