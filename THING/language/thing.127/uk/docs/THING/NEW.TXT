=======================================================================
Thing, 1.27                                                     NEW.TXT
Copyright (c) 1994-97 Arno Welzel and Thomas Binder

TOS2GEM and ThingWait (c) Thomas Binder
'Dcheneva' fonts (c) Thomas Schulze (ts@uni-paderborn.de)

UK support by Joe Connor of InterActive - see SUPPORT.TXT for details.
======================================================================

Here you will find a few important notes about this new version of Thing, 
both from the program technical as well as organisational point of view.

� The sorting mode of files and folders can now be set separately for 
  each directory window.

� The 'Thing Icon Manager' is ready at last, but I found it too big to 
  include in the main Thing archive -- so it has its own archive which 
  also makes separate updates easier. ThingIcn 1.10 should be available 
  from the same source as Thing itself (if you don't have it already); the 
  archive name is TICN110D.LZH for the German version and TICN110E.LZH for 
  the English language one.

� Thing 1.27 absolutely requires the new THINGREG.OVL that is included in 
  the archive, otherwise no windows will be opened when the program is 
  launched. A new key, on the other hand, is _not_ required.

� Thing 1.2x can no longer read configuration files from versions prior 
  to 0.59. Those still using such an old version should first of all use 
  the 1.09 version to save the configurations in the newer format. To do 
  this simply install 1.09 over the old version, start it once and 
  immediately save the settings. After this Thing 1.20 can be installed.

==========================================================================
As the English documentation for Thing 1.20 was not widely distributed, 
the new items in that version are included here for information:

� Thing 1.2x now costs 30 DM or �16 for new registrations in place of
  25 DM or �13 previously.

� Updates from Thing 1.0x are not free but subject to a charge of 15 DM 
  or �6.

� Registrations as well as updates are no longer handled by Arno Welzel, 
  but by Thomas Binder. For further details see REGISTER.TXT or UPDATE.TXT.

� Thing now has two separate resource files, thing.rsc and thingtxt.rsc, 
  both of which have to be copied to the Thing directory when updating.

� The new version of Thing uses a new registration process (thanks to 
  some successful hacks), which makes it necessary to also copy the file 
  THINGREG.OVL into the Thing directory. On this occasion I'd like to 
  remind you once more that a new key is needed for the update from V1.0x 
  (see UPDATE.TXT).


