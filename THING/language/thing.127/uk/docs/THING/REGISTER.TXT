=======================================================================
Thing, 1.27                                                REGISTER.TXT
Copyright (c) 1994-98 Arno Welzel and Thomas Binder

TOS2GEM and ThingWait (c) Thomas Binder
'Dcheneva' fonts (c) Thomas Schulze (ts@uni-paderborn.de)

UK support by Joe Connor of InterActive - see SUPPORT.TXT for details.
======================================================================

Thing is shareware!

Thing may be distributed freely in non-commercial channels. Thing may 
NOT under ANY circumstances be included in CD-ROM collections or on 
magazine cover/subscriber disks etc. without my prior written permission! 
Thing and its documentation must always remain together, complete with all 
its files and documentation. Changes to the program and/or documentation 
is forbidden.

Thing may be evaluated free of charge for 4 weeks. After that time 
registration is required, or you should remove Thing from your hard disk.

The shareware fee from Version 1.20 on is 30.00 DM or �16 and payment 
should now be made to Thomas Binder! UK and other non-German users may 
prefer to use Joe Connor's InterActive shareware scheme - see SUPPORT.TXT

For registrations I need, besides the above-mentioned payment, also the 
following registration form - either by Email or normal post. Name and 
address are definitely required and will naturally remain confidential!

After receiving the form and shareware fee I will send you a registation 
key by return, which will remove the nagging reminder on the desktop and  
the alert box when starting Thing ;-).

Registration keys may *not* be passed on to others! If you sell your 
computer or hard disk etc. please delete the file THING.KEY first!


Normal post:    Thomas Binder
                Johann-Valentin-May-Stra�e 7
                64665 Alsbach-H�hnlein

EMail:          Thomas Binder @ HD (MausNet, no mails > 16K!)
                gryf@hrzpub.tu-darmstadt.de (InterNet)

Bank account:   Thomas Binder
                A/c No.: 9 024 050 00
                Dresdner Bank AG Frankfurt am Main,
                BLZ (sort code): 500 800 00

----------------->  cut here <-------------------------


Registration for Thing
-----------------------

Version .......... : 1.27

Forename ......... :

Surname .......... :

Address .......... :

EMail ............ :




Many thanks!
