=======================================================================
Thing, 1.27                                                  README.TXT
Copyright (c) 1994-98 Arno Welzel und Thomas Binder

TOS2GEM und ThingWait (c) Thomas Binder
'Dcheneva' Schriften (c) Thomas Schulze (ts@uni-paderborn.de)

UK software support by Joe Connor - see SUPPORT.TXT for details.
=======================================================================

Thing ist Shareware! Mehr dazu in der Datei REGISTER.TXT.

Vielen Dank!


Leider hatten sich in Thing 1.26 kurzfristig einige unsch�ne Fehler 
eingeschlichen, so da� dieses relativ 'schnelle' Update n�tig wurde. 
Aus diesem Grund sind bei Thing 1.27 auch nur Fehler der 1.26 behoben 
worden - neue Funktionen gibt es diesmal nicht.

Das Update ist f�r registrierte Benutzer von Thing 1.20 oder h�her 
kostenlos, f�r registrierte Benutzer der Version 1.0x jedoch 
kostenpflichtig, siehe auch UPDATE.TXT! Besitzer der Psi-CD aus der 
Whiteline CD Series k�nnen ebenfalls updaten, die speziellen 
Konditionen hierf�r finden sich in README.PSI!

Auch der 'Thing Icon Manager' ist seit Thing 1.26 endlich fertig, 
allerdings war er mir zu gro�, um ihn auch noch mit ins Hauptarchiv 
aufzunehmen - au�erdem sind auf diese Weise getrennte Updates 
einfacher. ThingIcn 1.10 sollte, falls noch nicht vorhanden, aus der 
gleichen Quelle wie Thing selbst erh�ltlich sein, der Archivname ist 
TICN110D.LZH

Kontaktadressen:
---------------

  Weiterentwickler / Ansprechpartner f�r Registrierungen, Probleme und 
  Vorschl�ge:

  Thomas Binder
  Johann-Valentin-May-Stra�e 7
  64665 Alsbach-H�hnlein
  Deutschland

  EMail: binder@rbg.informatik.tu-darmstadt.de (InterNet)
         gryf@hrzpub.tu-darmstadt.de (dito)
         Thomas Binder @ HD (MausNet, keine Mails > 16K!)
  
  IRC: Gryf
  
  Bankverbindung: Thomas Binder
                  Dresdner Bank AG Frankfurt am Main
                  Konto-Nummer 9 024 050 00
                  BLZ 500 800 00

  Urspr�nglicher Autor (nicht mehr an Thing beteiligt):

  Arno Welzel
  Georgenstra�e 55
  86152 Augsburg

  EMail: welzel@augusta.de


ST-Guide ist erh�ltlich bei:
----------------------------
  Holger Weets
  Tangastra�e 45
  26121 Oldenburg

  EMail: Holger Weets @ OL (MausNet, keine Mails > 16K!)
  Holger_Weets@ol.maus.de (InterNet, gleiche Einschr�nkung wie oben!)

  Dazu mu� ich anmerken, da� ich an ST-Guide weder finanziell noch 
  sonst irgendwie beteiligt bin!
