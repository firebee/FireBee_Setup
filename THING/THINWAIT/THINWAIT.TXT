    Instructionsfor ThingWait of 28.08.1995
    ---------------------------------------


    What's that?
    ------------

    The following kept on happening to me: As soon as Thing (installed as 
    an auto-start application under Single-TOS) started a program working 
    in the overlay mode, my computer crashed with 2 bombs when I selected 
    an XControl CPX module. At first I blamed Thing, since nothing like 
    this had happened before using it. But when I recently took a closer 
    look at the problem it soon became clear that Thing had nothing to do 
    with this. The cause is connected with the auto-start mechanism and 
    the fact that XControl has a very long initialisation phase (above 
    all when, like me, you have a lot of modules active).

    As is fairly well known, memory allocated by desk accessories under 
    Single-TOS always belongs to the currently active main application. So 
    normally (without an auto-start application) all the memory allocated 
    initially by XControl belongs to the desktop, which then never 
    releases it, since it cannot be terminated. With Thing (in principle 
    other programs as well) as an auto-start application, matters look 
    rather different: It will usually be started before XControl completes 
    its initialisation phase; as a result a part of the XControl memory 
    (for GEMDOS) no longer belongs to the desktop but to Thing.

    If Thing now starts a program with the option 'Unload Thing on 
    starting programs (overlay)' switched on, Thing will terminate in 
    order to start ThingRun. At that moment all memory that XControl had 
    allocated when Thing was already running will be released again. In 
    unfavourable circumstances this can lead to data important to XControl 
    being overwritten because that part of the memory is now occupied by a 
    new program. This results in the crashes mentioned at the start of 
    this text.

    The solution is the included ThingWait, a mini-program that is 
    installed as an auto-start application in place of Thing. This waits 
    first for a few seconds (for configuration see below) in order to 
    give desk accessories like XControl time to initialise, and then 
    terminates to start Thing automatically. To prevent memory being 
    released on termination as normal, ThingWait uses Ptermres for this, 
    which induces GEMDOS to exclude from its memory list all the memory 
    that ThingWait (and also possibly the desk accessories) had allocated, 
    i.e. it can no longer be allocated as it is practically occupied 
    'permanently'.

    ThingWait only retains the most essential part (128 bytes) of its own 
    code in RAM, so in principle only the memory really occupied by any 
    desk accessories is held back. The happy result: XControl no longer 
    bombs as described, and possibly some other desk accessories are more 
    stable in conjunction with Thing.

    Those who program their own desk accessories that allocate memory 
    when starting and may (sometimes) need a little time to do this should 
    incidentally bracket the initialisation with wind_update because an 
    auto-start application can not be started while the screen is blocked. 
    This will ensure that the allocated memory belongs to the desktop and 
    is not released prematurely.


    Usage
    -----

    As already noted, ThingWait should be installed as an auto-start 
    application in place of Thing. For this it must be in the same 
    directory as THING.APP or be able to find Thing via the environmental 
    variable THINGDIR! The program name determines how many seconds
    ThingWait grants to the desk accessories for their initialisation. For 
    this the program name needs to contain a number somewhere in it.
    Examples: THINWT05.PRG -> Delay 5 seconds, 10THINWT.PRG -> 10 seconds,
    THING3WT.PRG -> 3 seconds. If no number is found, ThingWait will 
    assume 10 seconds by default. Incidentally the number must lie 
    between 1 and 30 (inclusive); if this is exceeded then the most  
    appropriate limit will be used.

    The number of seconds required will have to be determined by each 
    user, as it depends on the desk accessories used and the TOS version. 
    In principle 10 seconds should suffice for the most extreme cases, 
    although shorter times should be tried first as 10 seconds seems to 
    take ages and in any case (at least for me) booting _always_ takes too 
    long...


    Legal
    -----

    ThingWait was developed with great care and tested extensively; 
    nevertheless I cannot guarantee that it is completely free of errors.
    I can accept no liability for any sort of damage, no matter of what 
    kind, direct or indirect due to proper or improper use of ThingWait, 
    or its unsuitability for a certain purpose. You use it at your own 
    risk!


    Distribution
    ------------

    ThingWait can be copied and used freely, but distribution must always 
    include all the files (THINWAIT.APP and THINWAIT.TXT) complete and 
    unchanged.  ThingWait may also be distributed as part of the original
    Thing package, if Arno has taken it up. In this case THINWAIT.APP 
    belongs in the THING diectory and THINWAIT.TXT in the DOC directory.


    Author
    ------

    ThingWait was written in Pure C 1.0 by:

    Thomas Binder
    Johann-Valentin-May-Stra�e 7
    64665 Alsbach-H�hnlein
    Deutschland/Germany

    InterNet: binder@rbg.informatik.tu-darmstadt.de
    IRC: Gryf

    I am always approachable for questions, bug reports, suggestions etc.!


    Technical
    ---------

    As ThingWait terminates itself with Ptermres(128, 0), then in addition 
    to memory occupied by any desk accessories these 128 bytes and the 
    environment of ThingWait remain resident (it can't be less, as at 
    least the basepage (without command line) must remain). The following 
    example tables show, however, that by comparison this hardly matters:

    Free memory with Thing and ThingWait for my minimal setup with a few  
    AUTO-folder programs and XControl (TOS 4.04, 4MB memory):
    Block #1: 3693828 bytes
    Block #2: 9232 bytes
    Block #3: 5792 bytes
    Block #4: 256 bytes
    Summary: 3709108 bytes in 4 blocks

    Setup as above although without ThingWait (so as described at the 
    start, with crashing XControl):
    Block #1: 3711508 bytes
    Block #2: 5792 bytes
    Block #3: 256 bytes
    Summary: 3717556 bytes in 3 blocks

    Same setup, although completely without auto-start applications:
    Block #1: 3684580 bytes
    Block #2: 18720 bytes
    Block #3: 5792 bytes
    Block #4: 256 bytes
    Summary: 3709348 bytes in 4 blocks

    All measurements were made directly from the desktop, so after Thing 
    had terminated for the first two tables. One can see that with 
    ThingWait 240 bytes fewer are free than without any auto-start 
    applications. These 240 bytes are the basepage and environment of 
    ThingWait. Compared to the setup with Thing as auto-start application 
    there are even 8448 bytes missing. Of this 240 are again basepage and 
    environment of ThingWait, the remaining 8208 were allocated by 
    XControl while ThingWait waited. So these 8208 bytes essential for 
    XControl are lost without ThingWait and are overwritten as soon as 
    Thing starts up a program after unloading itself from memory.

    So in the end, compared to the 'safest' setup without any auto-start  
    applications where all desk accessory memory belongs to the desktop,
    one has lost only 240 bytes (with a larger default environment 
    correspondingly more), without fragmenting the memory any further. 
    I think this is quite bearable, specially as it restores full 
    operational safety.

    For people who get tears in their eyes at the loss of a single byte of 
    memory one could, with a suitable GEMDOS version, make the environment 
    and basepage of ThingWait available again via Maddalt as additional 
    alternate RAM. It's questionable whether this is worth it, especially 
    as it would require considerable effort...
