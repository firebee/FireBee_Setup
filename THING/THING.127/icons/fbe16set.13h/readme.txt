              FB.e 16 colors 48pts - Icon set for Thing! 1.27
             -----------------------------------------------
                       Version: 1.3H (2019/09/07) 

test version: do not redistribute!   
             
Introduction
------------
The 16 colors FB.essential  icons set for Thing! 1.27, was created for the
2019 FreeMiNT Setup for the FireBee computer. 

On  the FireBee, Thing! 1.27 doesn't support 256 colors icons. In addition,
256  colors  icons turn to  full black  when in 256 colors screen mode thus
the 16 colors FB.e icons subset was created to address these limitations. 
Users to enjoy a modern look for the Thing! desktop also when in 256 colors 
screen modes.  

This fb.e set is composed by 146 48p. icons in 16 and 2 colors and 133 mini
icons again in 16, 2 colors.

KNOWN LIMITATIONS AND ISSUES: 

- COLOR MINI ICONS CAN SUFFER REDRAW ISSUES WHEN THE WINDOW IS SCROLLED. 

Copyrights
----------
All icons: design by Lodovico Zanier

Version: 1.3H
Release date: 2019/09/07

License
-------
The icon set "FB.e 16 colors 48pts"  is licensed  by the Author  listed in 
the Copyrights section of this readme file, under a:

CREATIVE COMMONS  ATTIBUTION - NONCOMMERCIAL  - NO DERIVATES 3.0  UNPORTED
LICENSE (http://creativecommons.org/licenses/by-nc-nd/3.0/)

Under this licence, you are free:

to Share - to copy, distribute  and transmit the work under the  following
conditions:

Attribution - You must attribute the  work in the manner specified by  the
author or licensor (but not in any way that suggests that they endorse you
or your use of the work).

Noncommercial - You may not use this work for commercial purposes. 

No Derivative Works  - You may  not alter, transform,  or build upon  this
work. 

The full Creative Commons License is in the CC_Legal.txt document.  Please
note  that the summary.txt file, also included, has no legal value, and is 
supplied to help to understand the  full Legal Code (CC_Legal.txt).


Archive contents
----------------
icons.rsc   : Color icons file (icons available in 16 & 2 colors);
icons.inf   :  icons assignment file for Thing!;
readme.txt  : This file;
summary.txt : Handy reference for  understanding the Legal Code (the  full
              license in CC_Legal.txt);
CC_Legal.txt: Creative Commons license (CC BY-NC-ND);

DISCLAIMER
----------
This Icon Set  is  PROVIDED  AS  IT  IS,  and  the  author  DISCLAIM   ALL
RESPONSIBILITY  FOR ANY LOSS OR DAMAGE  resulting from  the use, no matter
how it is caused.
 
Notice
------
For the desktop's  operating instructions,  please   refer  to the  Thing!
documentation.  With the  "Edit Icon Assignment" menu  item it is possible
to edit icons assignments to files, folders and drives.

For freemint/xaaes  or  freemint/myaes OS, please  refer to  the  official
distributions documents and the FreeMiNT wiki.

Updates
-------
Updates will be available at the http://firebee.org web page.


Support
-------
For support please post/send your questions:

- At the http://firebee.org web page 

- at the FireBee forum on "Atari-Forum" (subscription required):

http://www.atari-forum.com/viewforum.php?f=92

- to my email address: lzanier(at)firebee.org


THANKS:
- to the  ACP group, for their unvaluable suggestions and support.
- to the beta testers.
