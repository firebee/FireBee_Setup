Creative Commons License Summary

Creative Commons Attribution-NonCommercial-NoDerivates 3.0 Unported 
(CC BY-NC-ND 3.0)  


You are free:
to Share - to copy, distribute and transmit the work 

Under the following conditions:

Attribution - You must attribute the work in the manner specified by the 
author or licensor (but not in any way that suggests that they endorse you 
or your use of the work).

Noncommercial - You may not use this work for commercial purposes. 

No Derivative Works - You may not alter, transform, or build upon this work. 


With the understanding that: 

Waiver - Any of the above conditions can be waived if you get permission from 
the copyright holder.

Public Domain - Where the work or any of its elements is in the public domain 
under applicable law, that status is in no way affected by the license.
 
Other Rights - In no way are any of the following rights affected by the 
license:
 
* Your fair dealing or fair use rights, or other applicable copyright 
  exceptions and limitations; 
* The author's moral rights;
* Rights other persons may have either in the work itself or in how the work
  is used, such as publicity or privacy rights. 


Disclaimer

The summary document is not the license. It is simply a handy reference 
for understanding the Legal Code (the full license) � it is a 
human-readable expression of some of its key terms. Think of it 
as the user-friendly interface to the Legal Code beneath. This Deed
 itself has no legal value, and its contents do not appear in the 
actual license. 

Creative Commons is not a law firm and does not provide legal services. 
Distributing of, displaying of, or linking to this Commons Deed does not
create an attorney-client relationship. 