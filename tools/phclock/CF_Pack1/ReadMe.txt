

                      Clock Face Extension Pack

                                  by

                            AtFaCT Factory

                                 for

                          WongCK's PH Clock


                       Last Updated 30/11/2012



Introduction
------------

For some time now, WongCK has been again producing software for 
classic Atari TOS machines. He is totally open-minded to suggestions 
by users. It is awesome what he has done so far, and this package is 
supposed to support this fantastic work.


What is it?
-----------

This extension pack contains about 24 clock faces for the great PH 
Clock by WongCK. All of them are clippings and/or scaled versions of 
existing pictures. I tried to find the best ones and to make them 
into perfect clock faces.

Only Atari machines (Falcon with CT60) and software were used to 
create this package.


Some Info on the Clock Faces
----------------------------

01     White Atari logo on orange background. The picture resembles 
       the modern Apple look.
02     Clipping of the cool red Atari logo on black background from a 
       picture by Enhanced Buzz.
03     Clipping of an awesome Atari wallpaper by tominosik1.
04     Wonderful artistic Atari logo by SikkinDiVigil.
05     Clipping of an Atari poster.
06     Clipping of the nice Atari wallpaper mATARIx. An Atari logo 
       designed like the one of the science fiction movie The Matrix.
07     Cool Atari logo by Kamorek called Atari Air Show.
08     Clipping of a well-designed Atari wallpaper by GameScanner.
09     Clipping of a great Atari wallpaper by InvertedInferno.
10     Logo of the famous Atari ST game company Thalion. They 
       released games like Wings of Death or No Second Prize.
11-14  Four different Motorola logos. This company produced the 68k 
       processor family (Atari ST, TT, Falcon, clones and accelerator 
       boards).
15-24  Ten clocks by the German clock manufacturer Vaerst. The 
       pictures have been kindly provided by them. Their fine clocks 
       are made from natural state. If you like them, please visit 
       www.vaerst.de.

All images have a size of 120x120 pixels, so PH Clock do not have to 
scale the pictures.


Installation
------------

In the folder 'TC', you find the clock faces in true colour PNG 
format (16 million colours). For resolutions with just 256 colours (8-
bit screen modes), you can use special 256-colour PNG versions of the 
pictures that can be found in folder '256C'. They were adapted in a 
way they look as good as possible with the conversion routine of the 
LDG plugin that PH Clock makes use of.

Some rough in-depth information on colour reduction: Pictures with 
sharp contrasts were dithered with the nearest-colour algorithm of a 
converter and modified manually with a pixel painter since the LDG 
system still dithered some colours. I replaced most of the dithered 
colours with the best-matching non-dithered one. In order to know 
which ones are not dithered by the LDG routine, in an 8-bit 
resolution, I displayed a picture with all 256 screen colours with 
zView (also uses LDG system). Finally, I converted the pictures with 
zView. As continuous-tone images look good with the LDG routine, they 
were converted only with it.

You just have to replace the original clock faces in PH Clock's clock 
face directory by the true- or 256-colour clock faces of this package.

As all 256-colour pictures are converted for the NVDI 5 colour 
palette, your desktop has to set this palette or you must install the 
Atari VDI replacement NVDI 5, else the clock faces will not look 
as intended.


Disclaimer
----------

If any of the clock faces images/logo used infringe on any copyright 
laws, please inform us at the e-mail address below and we will 
promptly remove those images from the download package.


Contact
-------
If you have any questions or comments regarding the clock faces, or 
any other Atari related stuff, or you want me to add further clock 
faces to this package, you can contact me via e-mail at 
atfact@arcor.de.