

PH Clock is a simple program that displays the time. 
It features changable graphical clock face, coloured hands, chime and alarm.
The sound of the alarm and chime can be any sound file of your choice.
Integrated to it is a sample player playing Wave, Au, SND etc.

Please see the short documentation inside the DOC folder for installation.

Thanks to Latz for Geman translation, AtFaCT for icons.
Thanks !!

Wongck
May 2017
