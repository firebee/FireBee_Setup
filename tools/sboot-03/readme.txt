		SBOOT 0.3



	Startup Manager for the Firebee...
	... and other TOS compatibles.






 Features:
==========



- Configurable timeout & Folder settings for: PRG,CPX,ACC
- Comfortable & fast console User interface.
- Up to 10 different Sets which can be selected by F1-10 keys. 
- Change the startup order of your AUTO folder Programs. 
- Custom startup script for each Set (since version 0.2). 
  Supported commands since version 0.2: 
	copy, echo, pause, set echo off/on (similar to DOS batch ) 
  Supported commands since version 0.3:
	rewrite 






 Installation:
===============



- Simply copy SBOOT.PRG into your AUTO folder
- press "C" for initial setup 
- move SBOOT.PRG to the top of program list.



 Keybindings:
==============
 
 Mainscreen:



Help:           Show this screen



Enter:          Toogle active/inactive



ESC/Q/q:        Continue boot / save changes



Shift+A:        Move file up



Shift+Y:        Move file down



Shift+F1-F10:   Save Set 1-10



F1-F10:         Load Set 1-10



C:              Configuration



D:              Delete current set



E:              Edit Script for current set



L:				List available sets 



S:              Save without exit



 Editor:



ESC:			Exit without save

F2: 			Save data






 Script Commands:
==================



 (optional parameters marked with *)



pause *message   -  waits for any keypress



echo *message    -  prints message to screen or just newline



set echo off     -  turn on/off verbose command output



copy src dst     -  copy file from source to destination



rewrite file needle replacement - Replace all occurences of "needle" 
                                  in file X with a string "replacement".
				  					(this command is not case sensitive)
