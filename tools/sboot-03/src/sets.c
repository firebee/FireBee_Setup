#include <tos.h>
#include <vdi.h>
#include <time.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>

#include "sets.h"
#include "ezxml/ezxml.h"

extern ezxml_t ezxmlh;

ezxml_t get_set_by_pos( short pos )
{
	ezxml_t lset, res;
	char * tpos;
	
	res = NULL;
	
	for( lset = ezxml_child(ezxmlh, "set"); lset; lset = lset->next){
		tpos = (char*)ezxml_attr(lset, "pos");
		if( tpos != NULL ) {
			if( ((short)atoi(tpos)) == pos ) {
				res = lset;
				break;
			} 
		}
	}
	return( res );
}