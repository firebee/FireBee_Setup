#ifndef EDITOR_H

#define EDIT_BUF_DEFAULT_BLOCKSIZE 255;

struct s_edit_buf {
	char * data;
	unsigned long used;
	unsigned long size;
	int blocksize;
	int lines;
	unsigned long scroll_x;
	unsigned long scroll_y;
	unsigned long cursor;
	long redraw_start_at_char;
	long redraw_limit_lines;
	long redraw_limit_chars;
};

int edit_buf_init( struct s_edit_buf * buf);
void edit_buf_destroy( struct s_edit_buf * buf );
char * edit_text( char * title, char * text );
void end_edit_text( void );

/*void editor_input( char c, unsigned long where );
void editor_scroll_x( int n, unsigned char abs );
void editor_scroll_y( int n, unsigned char abs );
void editor_place_caret( unsigned long x, unsigned long y );
*/
#endif