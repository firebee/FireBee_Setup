#ifndef LINEA_H
#define LINEA_H


#define LINEA_BYTES_PER_ROW	-2
#define LINEA_RES_W			-0x0C
#define LINEA_RES_H			-0x04
#define LINEA_PLANES		0
#define LINEA_CELL_H		-0x02E

extern long lineavar;

void cdecl linea_init(void);
void cdecl linea_init_cf(void);

#endif