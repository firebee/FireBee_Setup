#include <tos.h>
#include <vdi.h>
#include <time.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>

#include "ezxml/ezxml.h"

ezxml_t load_set( short idx );
ezxml_t get_set_by_pos( short pos );