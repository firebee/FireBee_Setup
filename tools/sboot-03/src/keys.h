#ifndef KEYS_H
#define KEYS_H

/* scancodes: */
#define F11_BASE	(84<<8)
#define F1_BASE		(59<<8)
#define F1 			(59<<8) 
#define F2 			(60<<8)
#define F3 			(61<<8)
#define CURS_UP 	(72<<8)
#define CURS_DOWN 	(80<<8)
#define CURS_RIGHT 	(77<<8)
#define CURS_LEFT	(75<<8)
#define BACKSPACE	0x0E00
#define CURS_S_UP	0x4838
#define CURS_S_DOWN 0x5032
#define CTRL_S		0x1F00
#define HELP 		0x6200
#define ESC			0x100
#define ENTER		(13<<8)
#define RETURN		(13<<8)

#endif