#ifndef GUI_H
#define GUI_H

#define VERSION 	"0.2 - " __DATE__

extern short vdih;
extern short vdiph;
extern short vdivh;
extern short workin[16];
extern short workout[58];

/* textmode resolution: */
extern short c_rows;
extern short c_cols;

extern short cell_h;
extern short cell_w;

extern short res_w;
extern short res_h;

extern void * screen;
extern short screen_bpp;

void init_gui( void );
void textxy(short x, short y, short inv, char * str);
void fillspace(short x, short y, short w, short black);
void draw_title( char * txt );
void draw_info( char * txt );
void clear_workarea( void );
short getkey( char * shift, short * keycode, char * ascii );


#endif