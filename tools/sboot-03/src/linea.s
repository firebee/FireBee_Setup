
GLOBL linea_init
GLOBL linea_init_cf
GLOBL lineavar

.text

;linea_initXY:
;	movem.l d0-d2/a0-a2,-(A7)
;	dc.w $A000
;	move.l a0,lineavar 
;	movem.l (sp)+,d0-d2,a0-a2
;	rts


linea_init: 
	lea -24(sp),sp
	movem.l d0-d2/a0-a2,(sp)
	dc.w $A000
	move.l a0,lineavar
	movem.l (sp),d0-d2/a0-a2
	lea 24(sp),sp
	rts

linea_init_cf: 
	lea -24(sp),sp
	movem.l d0-d2/a0-a2,(sp)
	dc.w $A920
	move.l a0,lineavar
	movem.l (sp),d0-d2/a0-a2
	lea 24(sp),sp
	rts


.data

lineavar:
ds.l 1