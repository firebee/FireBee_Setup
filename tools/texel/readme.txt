Texel 2.20 (07.03.99)
=====================

Wichtig:
========
Falls Sie kein MagiC 6.0 besitzen:
F�r das neue Texel m�ssen Sie das mitgelieferte WDIALOG.PRG in den AUTO-
Ordner kopieren. Beachten Sie, da� Sie es zus�tzlich in der AUTOEXEC.BAT 
eintragen m�ssen, falls Sie eine solche Datei in ihrem AUTO-Ordner 
vorhanden haben (z.B. bei MagiC-PC der Fall).

WDIALOG.PRG finden Sie im TEXEL-Verzeichnis.

Im Ordner TEXEL\OLGA\OLGATOOL finden Sie ein Programm, mit dem Sie 
die OLGA.INF auf Korrektheit �berpr�fen lassen k�nnen. Falls es  
Probleme beim Einbetten oder darstellen/bearbeiten von eingebetteten 
Objekten gibt, k�nnen Sie mit diesem Programm die OLGA.INF �berpr�fen 
lassen. Vermutlich findet das Programm die fehlerhafte Stelle.

Weitere Neuerungen dieser Version k�nnen Sie der ST-Guide-Online-Hilfe 
entnehmen. Rufen Sie dazu im Inhaltsverzeichnis "Was ist neu..." und 
danach "... in Version 2.20" auf.


Application Systems Heidelberg Software GmbH
Postfach 10 26 46
69016 Heidelberg
Tel: 06221/300002
Fax: 06221/300389
Mailbox 06221/303671
Internet: http://www.ash.sww.net
