All unpackers built for the ColdFire by Vincent Riviere.
Sources and binaries downloaded from: 

http://vincent.riviere.free.fr/soft/m68k-atari-mint/archives/mint/bzip2/
http://vincent.riviere.free.fr/soft/m68k-atari-mint/archives/mint/gzip/
http://vincent.riviere.free.fr/soft/m68k-atari-mint/archives/mint/lha/
http://vincent.riviere.free.fr/soft/m68k-atari-mint/archives/mint/tar/
http://vincent.riviere.free.fr/soft/m68k-atari-mint/archives/mint/unrar/
http://vincent.riviere.free.fr/soft/m68k-atari-mint/archives/mint/unzip/
http://vincent.riviere.free.fr/soft/m68k-atari-mint/archives/mint/zoo/
