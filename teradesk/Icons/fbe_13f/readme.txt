              FB.essential 48pts - Icon set for the Teradesk
             -----------------------------------------------
                       Version: 1.3f (2019/07/27)    
             
Introduction
------------
The FB.essential icons set  for the Teradesk desktop was developed for the 
2019 FreeMiNT Setup for the FireBee computer. 

This icon set, is composed by 141 48p. icons in 256, 16 and 2 colors.

Copyrights
----------
All icons: design by Lodovico Zanier (Ldv01)

Release date: 2019/07/27


License
-------
The icon set "FB.essential 48pts" is licensed by  the Author listed in the 
Copyrights section of this readme file, under a:

CREATIVE COMMONS  ATTIBUTION - NONCOMMERCIAL  - NO DERIVATES 3.0  UNPORTED
LICENSE (http://creativecommons.org/licenses/by-nc-nd/3.0/)

Under this licence, you are free:

to Share - to copy, distribute  and transmit the work under the  following
conditions:

Attribution - You must attribute the  work in the manner specified by  the
author or licensor (but not in any way that suggests that they endorse you
or your use of the work).

Noncommercial - You may not use this work for commercial purposes. 

No Derivative Works  - You may  not alter, transform,  or build upon  this
work. 

The full Creative Commons License is in the CC_Legal.txt document.  Please
note  that the summary.txt file, also included, has no legal value, and is 
supplied to help to understand the  full Legal Code (CC_Legal.txt).

The configuration file "teradesk.inf" is licensed under the GPL2  (see the
COPYING document).

Archive contents
----------------
cicons.rsc  : Color icons file (icons available in 256, 16 & 2 colors);
icons.rsc   : B&W only icons;
teradesk.inf: configuration file for Teradesk;
readme.txt  : This file;
summary.txt : Handy reference for  understanding the Legal Code (the  full
              license in CC_Legal.txt);
CC_Legal.txt: Creative Commons license (CC BY-NC-ND);
COPYING     : GNU General Public Licence, Version 2, June 1991 .

DISCLAIMER
----------
This Icon Set  is  PROVIDED  AS  IT  IS,  and  the  author  DISCLAIM   ALL
RESPONSIBILITY  FOR ANY LOSS OR DAMAGE  resulting from  the use, no matter
how it is caused.
 
Notice
------
For the desktop's  operating instructions,  please refer  to the  Teradesk
documentation.

For freemint/xaaes  or  freemint/myaes OS  please  refer to  the  official
distributions documents and the FreeMiNT wiki.

Updates
-------
Updates will be available at the http://firebee.org web page


Support
-------
For support please post/send your questions:

- At the http://firebee.org web page 

- at the Atari-Coldfire mail list (subscription required):

https://lists.lnxnt.org/mailman/listinfo/atari-coldfire

- at the FireBee forum on "Atari-Forum" (subscription required):

http://www.atari-forum.com/viewforum.php?f=92

- to my email address: lzanier(at)firebee.org


THANKS:
- to the current Teradesk mantainer.
- to the  ACP group, for their unvaluable suggestions and support.
- to the beta testers.
