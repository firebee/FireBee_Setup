KeyEdit
-------

A simple GEM utility to edit the keyboard layout tables used by 
FreeMiNT. Needs FreeMiNT >= 1.16 with XaAES or N.AES. Might also work 
with MyAES, but I this has not been tested.

KeyEdit is especially suited for ARAnyM users, as there's a huge 
variety in keyboards for PC's and Macs and many of them doesn't match 
the pre-defined keyboard tables in TOS/FreeMiNT.

Use ST-Guide or HypView to read user manual keyedit.hyp.

In folder "resource" you find different translations for KeyEdit.
Rename english default "keyedit.rsc" to ".rsx" and copy/rename your
preferred version as "keyedit.rsc" into the main program folder.

KeyEdit is freeware. Bug reports and suggestions for improvements are 
welcomed :-)

Jo Even Skarstein
<joska@online.no>