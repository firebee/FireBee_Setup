     BG_BLUE_STRUCTURE - Background Images for the FireBee Computer
     -------------------------------------------------------------- 
                    
            
Introduction
------------
Thank you for downloading the BG_BLUE_STRUCTURE set of background images 
for the FireBee computer. The supplied images  can be used as background  
image in  the  FreeMiNT/XaAES or  the  FreeMiNT/MyAES  environments. The 
following sizes are available: 4x3 (1600x1200), 16x9 (1920x1080),  16x10 
(1920x1200), 1024x768, 1280x1024.

Authors
----------
FireBee Logo idea: Friederike Dreyer (& Mathias Wittau)
FireBee Logo artwork: Jean-Pierre Feicht
BG concept & design: Mathias Wittau 
The ACP Team

LICENSE: CC BY-NC-SA 4.0

Version: 1.0
Release date: 2015/09/10


DISCLAIMER
----------
This  archive  is  PROVIDED  AS  IT  IS,  and  the  authors  DISCLAIM  ALL
RESPONSIBILITY FOR ANY LOSS OR DAMAGE  resulting from the use of this pack
of backgrounds, no matter how it is caused.


How to set a BG image
---------------------

FreeMiNT/XaAES
--------------
XaAES is capable  of displaying  a wallpaper  in all  colour depths  and
resolutions. However, *setting* a wallpaper is not straightforward,  but
it is done by pressing a keys combination which takes a snapshot of  the
current screen and  set it as  the desktop background.  As you  probably
don't want your GEM desktop as wallpaper, you need a program capable  of
displaying a picture in fullscreen, and then set the desktop background.

1.Open the picture of your choice  in zView, the image viewer  available
  in your FireBee setup. It's recommended to use the same proportions of
  your current screen res.

2.Press 'F10' to display the picture in zView fullscreen mode. 

3.Press the following keyboard combination:

CONTROL+ALT+":"

You might have to press  Shift to get the  ":", e.g. Shift+".". In  this
case, the keyboard combination will be Control+Alt+Shift+".". Now  XaAES
saves this picture as background in:

c:/mint/1-19-cur/xaaes/<resolution.colour>/xa_form.mfd 

4.Close zView. The picture formerly displayed  in fullscreen by zView is 
now  your desktop background. 

Please note that  this background will  be used in  your current  colour
depth and screenres only.  If you switch to  another resolution and  you
want to  keep the  same wallpaper,  you will  have to  repeat the  steps
above. The generated backgrounds  will be available  at every resolution
change. 

FreeMiNT MyAES
--------------
In MyAES,  the BG  image is  always  named desk.jpg  and by  default  is
located inside the skin=C:\gemsys\myaes\skins\<theme> folder. This  path
is set in the myaes configuration file and can be changed.

Be aware that the  skin\<theme> folder contains all  the images for  the
window's theme  in  use,  and  modifying the  contents  other  than  the
desk.jpg image, will affect the overall look.

Setting up a BG image
----------------------
1. Select the source BG image matching your preferred screenres;
2. Rename or Copy/Rename it into desk.jpg;
3. Move or Rename or Delete the current BG image, by default:
   (C:\gemsys\myaes\skins\<theme>\desk.jpg);
4. Copy/move the new desk.img to the skin/<theme> folder;
5. Reboot.

The selected  desk.img image  will be  used in  all screenres  as  MyAES
will  automatically  tile the background  images  smaller  than  current 
screenres and crop the right/bottom edges of those bigger.


System Requirements
-------------------
- A poweful TOS platform with TC screen depth;
- A Multitasking OS  (such as  FreeMiNT/XaAES or  FreeMiNT/MyAES) and  a
  desktop;
- the OS extension NVDI or the NVDI palette is recommanded (required  to
  display correctly the 256 colors icon sets for the fireBee).


Licence
-------
This background images  set is  licensed by  the Authors  listed in  the
"Authors" section of this readme file, under a:

CREATIVE COMMONS  ATTIBUTION-NONCOMMERCIAL-SHAREALIKE 4.0  INTERNATIONAL
(CC BY-NC-SA 4.0)

https://creativecommons.org/licenses/by-nc-sa/4.0/legalcode

Under this licence, you are free to:

Share - copy and redistribute the material in any medium or format
Adapt - remix, transform, and build upon the material 

The licensor cannot  revoke these  freedoms as  long as  you follow  the
license terms.

Under the following terms:

Attribution - You must  give appropriate credit, provide  a link to  the
license, and  indicate  if changes  were  made. You  may  do so  in  any
reasonable manner,  but  not  in  any way  that  suggests  the  licensor
endorses you or your use.

NonCommercial - You may not use the material for commercial purposes.

ShareAlike - If you  remix, transform, or build  upon the material,  you
must distribute  your  contributions  under  the  same  license  as  the
original.

No  additional  restrictions  -  You  may  not  apply  legal  terms   or
technological measures that legally restrict others from doing  anything
the license permits.


Notices: 
You do not have to comply with the license for elements of the  material
in the public  domain or where  your use is  permitted by an  applicable
exception or limitation.

No warranties  are  given. The  license  may not  give  you all  of  the
permissions necessary for your intended  use. For example, other  rights
such as publicity, privacy,  or moral rights may  limit how you use  the
material.

For the full text of the licence see the LICENSE.TXT document.


