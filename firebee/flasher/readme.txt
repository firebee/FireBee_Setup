Flashing the FireBee
--------------------

The FireBee's system software - that includes the FPGA firmware, the BaS,
FireTOS and EmuTOS - is stored in flash memory. The FireBee has 8MB of flash
memory which can be updated from TOS or MiNT by software.

Although there are no known problems when flashing the ROM under MiNT, we
recommend to perform this operation in a clean FireTOS or EmuTOS environment,
so the risk of "interferences" from other running tasks is kept to
a minimum while flashing.

The system software files can be downloaded from http://firebee.org.


FLASH_CF.PRG/FLASH060.PRG
-------------------------

There are two versions of the flash tool; FLASH_CF.PRG is a native ColdFire
compiled tool and can be used in both EmuTOS and FireTOS. The second,
FLASH060.PRG, is for the FireTOS environment only.

Flashing the "firmware" components is easy - start the program and click on
the harddrive-icon to open the fileselector. Select which file to flash.
You don't have to specify what kind of file it is (BaS, dBUG, FPGA, FireTOS,
EmuTOS...) as Flash* program will automatically detect this.

When you've selected a file, Flash program will load and verify it. This will
take a few seconds. When it's finished loading, click on the "Program" button.
The "LEDs" will light up one by one. When all are lit the programming has
finished and you can reboot.

NB! Don't flash the FireBee when it's running on battery power! If the
battery runs low during flashing you might end up with a "dead" FireBee and
external hardware is needed to reflash the flash memory!


FPGA firmware
-------------

The FPGA firmware specifies the behaviour of the FPGA. Inside the FPGA all
the Atari legacy components are implemented and most of the pins of most of
the FireBee's connectors/ports are directly connected to it.


BaS (Basis System) and dBUG
---------------------------

BaS or dBUG is the first thing that runs when the FireBee is switched on.
It initializes the hardware, loads the FPGA firmware into the FPGA and then
starts the operating system.
You can not install dBUG and BaS at the same time, it´s only possible to
use one of them.

dBUG has some additional features which are very handy if you're a developer.
First of all, it can boot an operating system (like TOS or Linux) from the
network. It also has a monitor which is accessible via the serial port.

BaS is written by Fredi Aschwanden.
dBUG by Freescale, adapted to the FireBee by Didier Mequignon.


EmuTOS
------

This is a version of EmuTOS built specifically for the FireBee and its
Coldfire CPU. As it is a Coldfire native OS, it is not capable of running
68k native programs, unless a 68K software emulator is used.
With the 68K emulator running 68K programs depends on several factors. As a
general rule, programs that do not break the Atari rules or relay on
undocumented/old/unofficial/deprecated features should run.


FireTOS
-------

FireTOS is the default Operating System for the FireBee and is derived
directly from the TOS60 for the CT6x Falcon accelerator. The FireTOS OS is
a heavily patched TOS 4.0.4 with the following additional key features:

- Support of most of the 68k opcodes with the help of the
  built-in CF68Klib (Freescale).
- Extended video modes via the built-in VDI driver for the FireBee Videl and
  ATI Radeon PCI graphics cards.
- USB drivers for keyboard, mouse and mass storage devices (memory sticks,
  USB harddrives...) that works under both TOS and FreeMiNT and also under
  EmuTOS when started from FireTOS boot menu.
- RAM-disk, TFTP-support, web-server, STiK compatible network stack.

Both EmuTOS and FireTOS have built-in harddisk drivers for the SD and CF cards.
They can access (and boot from) DOS-partitioned cards/drives.

The partition size limit for SD/CF cards is 2GB in FireTOS and EmuTOS with
the standard 8.3 Atari filenames.
In FreeMiNT, thanks to the VFAT option (set as default in our Setup),
disks/partitions of any size are possible and long filenames are supported.

Both EmuTOS and FireTOS are installed by default in the FireBee ROM. You can
select which one to start per default with setting the DIP switches on
the motherboard.

FireTOS: Switch #5 up, #6 up.
EmuTOS: Switch #5 down, #6 up.

EmuTOS started from switch is fully native and will access the Atari legacy
components only. This means that USB devices will not available, including
USB mice and keyboards.

ACP, May 2019.
http://firebee.org
