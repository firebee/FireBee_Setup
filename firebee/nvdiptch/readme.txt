How to patch NVDI 5.x to run on the FireBee
--------------------------------------------

This patch was successfully applied to NVDI versions 5.00, 5.01 and 5.03.
Your FireTOS has to be from 27th of July 2011 or newer - all new FireBees
has newer TOS-versions already.

1. Start up your FireBee into FireTOS

2. Install NVDI 5.x. If you install it using the installation program you
   must first use an other computer to copy all files *except* NEXTDISK 
   from both NVDI floppies to a SD-card/CF-card or USB-stick.
   If you have an other computer with NVDI 5.x installed, you can also copy
   this installation directly to your FireBee CF-card. If you do this, make
   sure that you're not including any third-party graphics card drivers!

3. Rename all NVDI*.SYS Files to NVDI*.SYX. This means you have to go to the
   folder C:/GEMSYS/ and rename the following files:
   * NVDIDRV1.SYS
   * NVDIDRV2.SYS
   * NVDIDRV4.SYS
   * NVDIDRV8.SYS
   * NVDIDRVH.SYS

4. Start the patch-program NVDIPTCH.TTP. The desktop will now ask for
   parameters - don't enter any (NVDIPTCH.TTP expects the following directories; 
   C:\AUTO\NVDI.PRG and C:\GEMSYS\OFF*.NOD ) except in the case where 
   your bootpartition isn't C:. If this is the case, enter the name of the
   partition (e.g. "d:"). 
   When you press ENTER, you will be asked to confirm each file to be
   patched. Answer "Yes" ("y" -> "enter") to all of these (normally 14 files).

   Now your patch is applied and you can leave the patch-program, the
   patching itself is now finished.

5. Now use ColdBoot (SBOOT.PRG in the autofolder) to move nvdi.prg *before*
   mint.prg. You can also sort the AUTO-folder manually. In any case NVDI
   has to be before MiNT and after ColdBoot.

Now you can reboot and enjoy the power of NVDI on your FireBee :)

(Patch program by Ole Loots/m0n0. Instructions by m0n0, Jo Even Skarstein
and Mathias Wittau.)
