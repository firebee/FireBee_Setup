# The FireBee FreeMiNT Setup

For those who want to run a maximum compatibility multitasking environment on the FireBee, we provide The FireBee FreeMiNT
Setup. This FreeMiNT setup is optimized for the FireBee and intended to use as a modern multitasking GEM environment . Means that there are only few basic UNIX commandline tools included (although you can add those features by yourself). Instead the setup contain "must have" GEM-programs for everyday using as a modern environment. 

## Features:
* FreeMiNT/XaAES coldfire kernel
* multitasking system, supporting long filenames and big partition sizes (FAT32 and ext2)
* pre-configured, automatic network connection with DHCP
* GEM Setup tools for easy system configuration
* Three selectable desktops (TeraDesk, Thing! 1.27 and 1.29) with modern iconsets.
* Browser NetSurf, Draconis ftp client, text editor QED, pdf and image viewer zView, music player mxPlay, helpsystem HypView, several development tools (AHCC, GBE, RecourceMaster, ...), controlpanel server COPS and more

##  Important notes: 
* NEVER try to run the FireBee FreeMiNT Setup with EmuTOS - it will crash!
* All the software included in this FireBee FreeMiNT Setup is provided "as is". Means that if you have problems/suggestions or just questions about a particular software, please read its documentation first and, if your issue is not solved, contact the author/developer directly or post in a forum.

All the installed programs are tested by several users and work good. But please understand that the ACP project can not maintain/support all this "third party" software!

Have fun!
